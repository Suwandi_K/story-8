AOS.init();
$('#activityButton').click(function(){
    $('#activityBody').toggleClass('hidden');
})
$('#experienceButton').click(function(){
    $('#experienceBody').toggleClass('hidden');
})
$('#achievementButton').click(function(){
    $('#achievementBody').toggleClass('hidden');
})
$('#contactButton').click(function(){
    $('#contactBody').toggleClass('hidden');
})

$('.accordion-button-up').click(function(){
    var thisAccordion = $(this).parent().parent().parent();
    thisAccordion.insertBefore(thisAccordion.prev());
})

$('.accordion-button-down').click(function(){
    var thisAccordion = $(this).parent().parent().parent();
    thisAccordion.insertAfter(thisAccordion.next());
})
$('#style-modifier').on('click', () => {
    $('.accordion-header').toggleClass('new-accordion');
    $('body').toggleClass('new-body');
    $('.buttons button').toggleClass('new-button')
});
