from django.test import TestCase, Client , LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import os
import time

from .views import index

# Create your tests here.
class HomepageUnitTest(TestCase):
    def test_home_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

class HomepageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

        super(HomepageFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(HomepageFunctionalTest, self).tearDown()


    def test_check_biodata_title_exist(self):
        checks = ['Aktivitas saat ini', 'Pengalaman organisasi/kepanitiaan', 'Prestasi', 'Kontak']

        self.browser.get(self.live_server_url + '/')

        for check in checks:
            self.assertIn(check, self.browser.page_source)

    def test_check_accordion_show_data_on_clicked(self):
        self.browser.get(self.live_server_url + '/')
        self.browser.implicitly_wait(10)

        self.assertIn("Suwandi's Profile", self.browser.title)

        profile = self.browser.find_elements_by_class_name('accordion-header')
        self.assertEqual(len(profile), 4)

        activity_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Aktivitas saat ini')][1]")
        activity_button.click()

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('1. Bangun tidur, mandi.', page_text)
        self.assertIn('2. Buka Line, liat grup, lalu kerjain tugas.', page_text)
        self.assertIn('3. Ngajak temen mabar ML.', page_text)

        organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Pengalaman organisasi/kepanitiaan')][1]")
        organizational_button.click()
        time.sleep(5)

        page_text_2 = self.browser.find_element_by_tag_name('body').text
        self.assertIn('1. KIR SMAN 2 Jakarta.', page_text_2)
        self.assertIn('2. KMBUI XXVIII.', page_text_2)
        self.assertIn('3. Waisak KMBUI.', page_text_2)
