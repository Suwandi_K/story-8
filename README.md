# Project Status
| Pipeline | Coverage |
| ------ | ------ |
|[![pipeline status](https://gitlab.com/Suwandi_K/story-8/badges/master/pipeline.svg)](https://gitlab.com/Suwandi_K/story-8/commits/master)| [![coverage report](https://gitlab.com/Suwandi_K/story-8/badges/master/coverage.svg)](https://gitlab.com/Suwandi_K/story-8/commits/master) |

# Story 8 PPW
Story 8 PPW Fasilkom UI Tahun Pelajaran 2019/2020 Semester Genap

# Data diri
<ol>
    <li>Nama    : Suwandi Kurniawan</li>
    <li>NPM     : 1906299175</li>
    <li>Kelas   : PPW</li>
    <li>Deployed at : https://story8-suwandi.herokuapp.com/</li>
</ol>